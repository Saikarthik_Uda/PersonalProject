<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Added User</title>
</head>
<body>
	<center>
		<h1 style="background-color: black; color: red">User Added Successfully</h1>
		<h1 style="background-color: black; color: red">User Created with ID: ${id}</h1>
		<h1 style="background-color: black; color: red">User Details</h1>
		<h2>UserName:${ub.getUname()}</h2>
		<h2>Mobile Number:${ub.getMobile()}</h2>
		<h2>Gmail:${ub.getMail()}</h2>
		<h2>Address:${ub.getAddress()}</h2>
		<a href="index.jsp">Home Page</a>
	</center>
</body>
</html>