package com.karthik.MyProject.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.karthik.MyProject.bean.UserBean;
import com.karthik.MyProject.service.UserService;

@org.springframework.stereotype.Controller
public class Controller {
	@Autowired
	UserService s;
	
	@RequestMapping("load.html")
	public ModelAndView load() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("SignUp.jsp");
		mv.addObject("userBean", new UserBean());
		return mv;
	}
	
	@RequestMapping("addUser.html")
	public ModelAndView addUser(@ModelAttribute("userBean") UserBean ub) {
		ModelAndView mv = new ModelAndView();
		Integer id = s.addUser(ub);
		mv.setViewName("addedUser.jsp");
		mv.addObject("ub", ub);
		mv.addObject("id", id);
		return mv;
	}
	
	@RequestMapping("ret.html")
	public ModelAndView addUser(@RequestParam int id) {
		ModelAndView mv = new ModelAndView();
		UserBean u = s.retrieveUser(id);
		mv.setViewName("retrieval.jsp");
		mv.addObject("ub", u);
		return mv;
	}
}
