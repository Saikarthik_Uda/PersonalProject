package com.karthik.MyProject.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.karthik.MyProject.entity.UserEntity;

public interface UserDao extends JpaRepository<UserEntity, Integer>{

}
