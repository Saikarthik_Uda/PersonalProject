package com.karthik.MyProject.dao;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.karthik.MyProject.bean.UserBean;
import com.karthik.MyProject.entity.UserEntity;

@Repository
public class DaoWrapper {
	@Autowired
	UserDao ud;

	public Integer addUser(UserBean ub) {
		UserBean b = new UserBean();
		UserEntity en = convertBeanToEntity(ub);
		b = convertEntityToBean(ud.save(en));
		return b.getId();
	}

	private UserBean convertEntityToBean(UserEntity save) {
		UserBean b = new UserBean();
		BeanUtils.copyProperties(save, b);
		return b;
	}

	private UserEntity convertBeanToEntity(UserBean ub) {
		UserEntity en = new UserEntity();
		BeanUtils.copyProperties(ub, en);
		return en;
	}

	public UserBean retrieveUser(Integer id) {
		UserBean b = new UserBean();
		UserEntity en = new UserEntity();
		en = ud.findById(id).orElse(null);
		b = convertEntityToBean(en);
		return b;
	}

	private UserBean convertEntityToBean(Optional<UserEntity> en) {
		UserBean b = new UserBean();
		BeanUtils.copyProperties(en, b);
		return b;
	}

}
