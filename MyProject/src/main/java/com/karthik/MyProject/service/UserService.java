package com.karthik.MyProject.service;

import com.karthik.MyProject.bean.UserBean;

public interface UserService {

	Integer addUser(UserBean ub);

	UserBean retrieveUser(Integer id);

}
