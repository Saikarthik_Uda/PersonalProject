package com.karthik.MyProject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.karthik.MyProject.bean.UserBean;
import com.karthik.MyProject.dao.DaoWrapper;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	DaoWrapper d;
	@Override
	public Integer addUser(UserBean ub) {
		return d.addUser(ub);
	}
	@Override
	public UserBean retrieveUser(Integer id) {
		// TODO Auto-generated method stub
		return d.retrieveUser(id);
	}

}
